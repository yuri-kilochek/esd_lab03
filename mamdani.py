#!/usr/bin/env python
# coding: utf8

import sys
import yaml
import argparse
import numpy as np
from pprint import pprint

argument_parser = argparse.ArgumentParser()
argument_parser.add_argument('knowledge', type=argparse.FileType('r'))
args = argument_parser.parse_args()

questions = yaml.load(sys.stdin)
knowledge = yaml.load(args.knowledge)

def eval_inc_func(params, x):
    a, b, c, d = params
    if x < a:
        return 0
    if x <= b:
        if a == b:
            return 0.5
        return (x - a) / (b - a)
    if x < c:
        return 1
    if x <= d:
        if c == d:
            return 0.5
        return (x - d) / (c - d)
    return 0

def ask(q_target_name, q_params):
    q_param_incs = {}
    for name, value in q_params:
        q_param_incs[name] = {}
        for lex_value, inc_func_params in knowledge['definitions'][name].items():
            q_param_incs[name][lex_value] = eval_inc_func(inc_func_params, value)
    target_inc_funcs_params = []
    for (r_target_name, target_lex_value), *conds in knowledge['rules']:
        if r_target_name != q_target_name:
            continue
        threshold = 1
        for cond_name, cond_lex_value in conds:
            cond_inc = q_param_incs[cond_name][cond_lex_value]
            threshold = min(threshold, cond_inc)
        params = knowledge['definitions'][r_target_name][target_lex_value]
        target_inc_funcs_params.append((threshold, params))

    def target_inc_func(x):
        return max(min(t, eval_inc_func(ps, x)) for t, ps in target_inc_funcs_params)

    start, end = +np.inf, -np.inf
    for t, (a, _, _, d) in target_inc_funcs_params:
        start, end = min(start, a), max(end, a)
        start, end = min(start, d), max(end, d)

    points = list(map(float, np.arange(start, end, (end - start) / 1000)))

    p = 0
    s = 0
    for a, b in zip(points, points[1:]):
        fa = target_inc_func(a)
        fb = target_inc_func(b)
        p += (b - a) * (fa + fb) / 2 * (a + b) / 2
        s += (b - a) * (fa + fb) / 2
    if s > 0:
        p /= s
    return p

answers = []
for target, *params in questions:
    answers.append([[target, ask(target, params)], *params])

yaml.dump(answers, sys.stdout)
