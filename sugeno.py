#!/usr/bin/env python
# coding: utf8

import sys
import yaml
import argparse
import numpy as np
from pprint import pprint

argument_parser = argparse.ArgumentParser()
argument_parser.add_argument('knowledge', type=argparse.FileType('r'))
args = argument_parser.parse_args()

questions = yaml.load(sys.stdin)
knowledge = yaml.load(args.knowledge)

def eval_inc_func(params, x):
    a, b, c, d = params
    if x < a:
        return 0
    if x <= b:
        if a == b:
            return 0.5
        return (x - a) / (b - a)
    if x < c:
        return 1
    if x <= d:
        if c == d:
            return 0.5
        return (x - d) / (c - d)
    return 0

def ask(q_target_name, q_params):
    q_param_incs = {}
    for name, value in q_params.items():
        q_param_incs[name] = {}
        for lex_value, inc_func_params in knowledge['definitions'][name].items():
            q_param_incs[name][lex_value] = eval_inc_func(inc_func_params, value)
    q_weight, q_activation = 0, 0
    for r_target_name, free_term, *conds in knowledge['rules']:
        if r_target_name != q_target_name:
            continue
        weight = 1
        activation = free_term
        for cond_name, cond_factor, cond_lex_value in conds:
            cond_inc = q_param_incs[cond_name][cond_lex_value]
            weight = min(weight, cond_inc)
            activation += q_params[cond_name] * cond_factor
        q_activation = q_activation * q_weight + activation * weight
        q_weight += weight 
        if q_weight > 0:
            q_activation /= q_weight
    return q_activation

answers = []
for target, *params in questions:
    answers.append([[target, ask(target, dict(params))], *params])

yaml.dump(answers, sys.stdout)
